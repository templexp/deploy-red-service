#!/usr/bin/env bash

sudo docker rm -f etcd-red-service
export PUBLIC_IP=`hostname -I | cut -d' ' -f1`
export NODE_NAME=ETCD-NODE-`hostname -I | cut -d' ' -f1|sed "s/\./-/g"`
sudo docker run -d -p 16801:16801 -p 16501:16501 --name  etcd-red-service  quay.io/coreos/etcd:v0.4.6 -peer-addr ${PUBLIC_IP}:16801 -addr ${PUBLIC_IP}:16501  -name ${NODE_NAME}   -peers 158.69.121.185:8001,158.69.121.185:8002,158.69.121.185:8003
sudo docker logs -f etcd-red-service &
sleep 5
curl -L ${PUBLIC_IP}:16501/v2/stats/leader
